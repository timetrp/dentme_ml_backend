from fastapi import APIRouter
from .pipeline import generate_qa
import textwrap

from pydantic import BaseModel

router = APIRouter(
    prefix="/quiz",
    tags=["quiz"],
    responses={404: {"description": "Not found"}}
)

class Book(BaseModel):
    content: str



@router.post("/generate")
async def generate_quiz(book: Book):

    # print(book.content, flush=True)
    content = book.content
    for token in ["\\r", "\\n", '\\"', '\r', '\n', '\"', '\\', "\\", '<pg_brk>', '{,,', ',,}']:
        content = content.replace(token, '')

    docs = textwrap.wrap(content, 800, break_long_words=False)
    quiz = []
    # print(docs, flush=True)
    for i,doc in enumerate(docs[:10]):
        print('quiz start...', str(i))
        doc_quiz = generate_qa(doc)
        quiz.extend(doc_quiz)
        print('quiz done...', str(i))

    
    return { "quiz_list": quiz}
    # return {"quiz_list": [1,2,3]}
