from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from qa import quiz
import uvicorn

app = FastAPI()

origins = [
    "http://localhost:3030",
    "https://localhost:3000",
    "http://localhost:5000",
    "http://localhost:8080",
    "http://localhost:8089",

]

app.include_router(quiz.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def main():
    return {"message": "Hello World"}


    
if __name__ == '__main__':
    uvicorn.run("main:app", port=8890, host='0.0.0.0', reload=True, timeout_keep_alive = 20)